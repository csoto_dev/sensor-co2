# from mq import *
import sys, time
from datetime import datetime

try:
    print("Press CTRL+C to abort.")
    # mq = MQ()
    while True:
        with open('log_tst.txt', 'a') as f:
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            # perc = mq.MQPercentage()
            f.write(dt_string + " -> LPG:c test ppm, CO: test ppm, Smoke: test ppm \n")
            time.sleep(3)
            f.close()
except:
    print("\nAbort by user")