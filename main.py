from mq import *
import sys, time
from datetime import datetime

try:
    print("Press CTRL+C to abort.")
    mq = MQ();
    while True:
        with open('log_sensor.txt', 'a') as f:
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            perc = mq.MQPercentage()
            log = str(dt_string) + " -> LPG: "+ str(perc["GAS_LPG"]) +" ppm, CO: "+ str(perc["CO"]) +" ppm, Smoke: "+ str(perc["SMOKE"]) +" ppm \n"
            f.write(log)
            f.close()
            time.sleep(3)
except Exception as e: 
    print(e)
    print("\nAbort by user")